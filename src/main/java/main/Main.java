package main;

import java.io.*;
import java.util.*;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentCatalog;
import org.apache.pdfbox.pdmodel.interactive.form.PDAcroForm;

public class Main {

    public static void main (String[] args) throws Exception {


            FormRecursoApelacion form = new FormRecursoApelacion();
            List<FormRecursoApelacion> list = new ArrayList<FormRecursoApelacion>();
            String filledForm = "C:\\Dxc\\pruebapdf.pdf";
            PDDocument document = PDDocument.load(new File("C:/Dxc/Formulario de Apelación_CC_template.pdf"));
            PDDocumentCatalog docCatalog = document.getDocumentCatalog();
            PDAcroForm acroForm = docCatalog.getAcroForm();

            form.setVarapelacion("dato_apelacion");
            form.setVarnombreapellido("dato_nombreapellido");
            form.setVartipodoc("dato_tipodoc");
            form.setVarnrodoc("dato_nrodoc");
            form.setVardireccion("dato_direccion");
            form.setVartelefono("dato_telefono");
            form.setVardistrito("dato_distrito");
            form.setVarprovincia("dato_provincia");
            form.setVardepartamento("dato_departamento");
            form.setVarnombreapellidopersona("dato_nombreapellidopersona");
            form.setVartipodocpersona("dato_tipodocpersona");
            form.setVarnrodocpersona("dato_nrodocpersona");
            form.setVarnombreapellidorepresentante("dato_nombreapellidirepresentante");
            form.setVartipodocrepresentante("dato_tipodocrepresentante");
            form.setVarnrodocrepresentante("dato_nrodocrepresentante");
            form.setVarnrorepresentante2("dato_nrodocrepresentante2");
            form.setVarcorreo("dato_correo");
            form.setVarnrotelefonoreclamo("dato_telefonoreclamo");
            form.setVarcodigo("dato_codigo");
            form.setVarrecursoapelacion("dato_recursoapelacion");
            form.setVarfecharesolucion("dato_fecharesolucion");
            form.setVarfechanotificacion("dato_fechanotificacion");
            form.setVarrazon1("dato_razon1");
            form.setVarrazon2("dato_razon2");
            form.setVarrazon3("dato_razon3");
            form.setVarrazon4("dato_razon4");
            form.setVarrazon5("dato_razon5");
            form.setVarrazon6("dato_razon6");
            form.setVarrazon7("dato_razon7");
            form.setVarrazon8("dato_razon8");
            form.setVarrazon9("dato_razon9");
            form.setVarrazon10("dato_razon10");
            form.setVarrazon11("dato_razon11");
            form.setVarrazon12("dato_razon12");
            form.setVarrazon13("dato_razon13");
            form.setVarrazon14("dato_razon14");
            form.setVarrazon15("dato_razon15");
            form.setVarrazon16("dato_razon16");
            form.setVarrazon17("dato_razon17");
            form.setVarfecha("dato_fecha");

            if (acroForm != null)
            {
                acroForm.getField( "varapelacion" ).setValue(form.getVarapelacion());;
                acroForm.getField("varnombreapellido").setValue(form.getVarnombreapellido());
                acroForm.getField("vartipodoc").setValue(form.getVartipodoc());
                acroForm.getField("varnrodoc").setValue(form.getVarnrodoc());
                acroForm.getField("vardireccion").setValue(form.getVardireccion());
                acroForm.getField("vartelefono").setValue(form.getVartelefono());
                acroForm.getField("vardistrito").setValue(form.getVardistrito());
                acroForm.getField("varprovincia").setValue(form.getVarprovincia());
                acroForm.getField("vardepartamento").setValue(form.getVardepartamento());
                acroForm.getField("varnombreapellidopersona").setValue(form.getVarnombreapellidopersona());
                acroForm.getField("vartipodocpersona").setValue(form.getVartipodocpersona());
                acroForm.getField("varnrodocpersona").setValue(form.getVarnrodocpersona());
                acroForm.getField("varnombreapellidorepresentante").setValue(form.getVarnombreapellidorepresentante());
                acroForm.getField("vartipodocrepresentante").setValue(form.getVartipodocrepresentante());
                acroForm.getField("varnrodocrepresentante").setValue(form.getVarnrodocrepresentante());
                acroForm.getField("varnrorepresentante2").setValue(form.getVarnrorepresentante2());
                acroForm.getField("varcorreo").setValue(form.getVarcorreo());
                acroForm.getField("varnrotelefonoreclamo").setValue(form.getVarnrotelefonoreclamo());
                acroForm.getField("varcodigo").setValue(form.getVarcodigo());
                acroForm.getField("varrecursoapelacion").setValue(form.getVarrecursoapelacion());
                acroForm.getField("varfecharesolucion").setValue(form.getVarfecharesolucion());
                acroForm.getField("varfechanotificacion").setValue(form.getVarfechanotificacion());
                acroForm.getField("varrazon1").setValue(form.getVarrazon1());
                acroForm.getField("varrazon2").setValue(form.getVarrazon2());
                acroForm.getField("varrazon3").setValue(form.getVarrazon3());
                acroForm.getField("varrazon4").setValue(form.getVarrazon4());
                acroForm.getField("varrazon5").setValue(form.getVarrazon5());
                acroForm.getField("varrazon6").setValue(form.getVarrazon6());
                acroForm.getField("varrazon7").setValue(form.getVarrazon7());
                acroForm.getField("varrazon8").setValue(form.getVarrazon8());
                acroForm.getField("varrazon9").setValue(form.getVarrazon9());
                acroForm.getField("varrazon10").setValue(form.getVarrazon10());
                acroForm.getField("varrazon11").setValue(form.getVarrazon11());
                acroForm.getField("varrazon12").setValue(form.getVarrazon12());
                acroForm.getField("varrazon13").setValue(form.getVarrazon13());
                acroForm.getField("varrazon14").setValue(form.getVarrazon14());
                acroForm.getField("varrazon15").setValue(form.getVarrazon15());
                acroForm.getField("varrazon16").setValue(form.getVarrazon16());
                acroForm.getField("varrazon17").setValue(form.getVarrazon17());
                acroForm.getField("varfecha").setValue(form.getVarfecha());
            }
            document.save(filledForm);
            document.close();
    }
}

