package main;

public class FormRecursoApelacion {
    private String varapelacion;
    private String varnombreapellido;
    private String vartipodoc;
    private String varnrodoc;
    private String vardireccion;
    private String vartelefono;
    private String vardistrito;
    private String varprovincia;
    private String vardepartamento;
    private String varnombreapellidopersona;
    private String vartipodocpersona;
    private String varnrodocpersona;
    private String varnombreapellidorepresentante;
    private String vartipodocrepresentante;
    private String varnrodocrepresentante;
    private String varnrorepresentante2;
    private String varcorreo;
    private String varnrotelefonoreclamo;
    private String varcodigo;
    private String varrecursoapelacion;
    private String varfecharesolucion;
    private String varfechanotificacion;
    private String varrazon1;
    private String varrazon2;
    private String varrazon3;
    private String varrazon4;
    private String varrazon5;
    private String varrazon6;
    private String varrazon7;
    private String varrazon8;
    private String varrazon9;
    private String varrazon10;
    private String varrazon11;
    private String varrazon12;
    private String varrazon13;
    private String varrazon14;
    private String varrazon15;
    private String varrazon16;
    private String varrazon17;
    private String varfecha;

    public FormRecursoApelacion() {
    }

    public FormRecursoApelacion(String varapelacion, String varnombreapellido, String vartipodoc, String varnrodoc, String vardireccion, String vartelefono, String vardistrito, String varprovincia, String vardepartamento, String varnombreapellidopersona, String vartipodocpersona, String varnrodocpersona, String varnombreapellidorepresentante, String vartipodocrepresentante, String varnrodocrepresentante, String varnrorepresentante2, String varcorreo, String varnrotelefonoreclamo, String varcodigo, String varrecursoapelacion, String varfecharesolucion, String varfechanotificacion, String varrazon1, String varrazon2, String varrazon3, String varrazon4, String varrazon5, String varrazon6, String varrazon7, String varrazon8, String varrazon9, String varrazon10, String varrazon11, String varrazon12, String varrazon13, String varrazon14, String varrazon15, String varrazon16, String varrazon17, String varfecha) {
        this.varapelacion = varapelacion;
        this.varnombreapellido = varnombreapellido;
        this.vartipodoc = vartipodoc;
        this.varnrodoc = varnrodoc;
        this.vardireccion = vardireccion;
        this.vartelefono = vartelefono;
        this.vardistrito = vardistrito;
        this.varprovincia = varprovincia;
        this.vardepartamento = vardepartamento;
        this.varnombreapellidopersona = varnombreapellidopersona;
        this.vartipodocpersona = vartipodocpersona;
        this.varnrodocpersona = varnrodocpersona;
        this.varnombreapellidorepresentante = varnombreapellidorepresentante;
        this.vartipodocrepresentante = vartipodocrepresentante;
        this.varnrodocrepresentante = varnrodocrepresentante;
        this.varnrorepresentante2 = varnrorepresentante2;
        this.varcorreo = varcorreo;
        this.varnrotelefonoreclamo = varnrotelefonoreclamo;
        this.varcodigo = varcodigo;
        this.varrecursoapelacion = varrecursoapelacion;
        this.varfecharesolucion = varfecharesolucion;
        this.varfechanotificacion = varfechanotificacion;
        this.varrazon1 = varrazon1;
        this.varrazon2 = varrazon2;
        this.varrazon3 = varrazon3;
        this.varrazon4 = varrazon4;
        this.varrazon5 = varrazon5;
        this.varrazon6 = varrazon6;
        this.varrazon7 = varrazon7;
        this.varrazon8 = varrazon8;
        this.varrazon9 = varrazon9;
        this.varrazon10 = varrazon10;
        this.varrazon11 = varrazon11;
        this.varrazon12 = varrazon12;
        this.varrazon13 = varrazon13;
        this.varrazon14 = varrazon14;
        this.varrazon15 = varrazon15;
        this.varrazon16 = varrazon16;
        this.varrazon17 = varrazon17;
        this.varfecha = varfecha;
    }


    public String getVarapelacion() {
        return varapelacion;
    }

    public void setVarapelacion(String varapelacion) {
        this.varapelacion = varapelacion;
    }

    public String getVarnombreapellido() {
        return varnombreapellido;
    }

    public void setVarnombreapellido(String varnombreapellido) {
        this.varnombreapellido = varnombreapellido;
    }

    public String getVartipodoc() {
        return vartipodoc;
    }

    public void setVartipodoc(String vartipodoc) {
        this.vartipodoc = vartipodoc;
    }

    public String getVarnrodoc() {
        return varnrodoc;
    }

    public void setVarnrodoc(String varnrodoc) {
        this.varnrodoc = varnrodoc;
    }

    public String getVardireccion() {
        return vardireccion;
    }

    public void setVardireccion(String vardireccion) {
        this.vardireccion = vardireccion;
    }

    public String getVartelefono() {
        return vartelefono;
    }

    public void setVartelefono(String vartelefono) {
        this.vartelefono = vartelefono;
    }

    public String getVardistrito() {
        return vardistrito;
    }

    public void setVardistrito(String vardistrito) {
        this.vardistrito = vardistrito;
    }

    public String getVarprovincia() {
        return varprovincia;
    }

    public void setVarprovincia(String varprovincia) {
        this.varprovincia = varprovincia;
    }

    public String getVardepartamento() {
        return vardepartamento;
    }

    public void setVardepartamento(String vardepartamento) {
        this.vardepartamento = vardepartamento;
    }

    public String getVarnombreapellidopersona() {
        return varnombreapellidopersona;
    }

    public void setVarnombreapellidopersona(String varnombreapellidopersona) {
        this.varnombreapellidopersona = varnombreapellidopersona;
    }

    public String getVartipodocpersona() {
        return vartipodocpersona;
    }

    public void setVartipodocpersona(String vartipodocpersona) {
        this.vartipodocpersona = vartipodocpersona;
    }

    public String getVarnrodocpersona() {
        return varnrodocpersona;
    }

    public void setVarnrodocpersona(String varnrodocpersona) {
        this.varnrodocpersona = varnrodocpersona;
    }

    public String getVarnombreapellidorepresentante() {
        return varnombreapellidorepresentante;
    }

    public void setVarnombreapellidorepresentante(String varnombreapellidorepresentante) {
        this.varnombreapellidorepresentante = varnombreapellidorepresentante;
    }

    public String getVartipodocrepresentante() {
        return vartipodocrepresentante;
    }

    public void setVartipodocrepresentante(String vartipodocrepresentante) {
        this.vartipodocrepresentante = vartipodocrepresentante;
    }

    public String getVarnrodocrepresentante() {
        return varnrodocrepresentante;
    }

    public void setVarnrodocrepresentante(String varnrodocrepresentante) {
        this.varnrodocrepresentante = varnrodocrepresentante;
    }

    public String getVarnrorepresentante2() {
        return varnrorepresentante2;
    }

    public void setVarnrorepresentante2(String varnrorepresentante2) {
        this.varnrorepresentante2 = varnrorepresentante2;
    }

    public String getVarcorreo() {
        return varcorreo;
    }

    public void setVarcorreo(String varcorreo) {
        this.varcorreo = varcorreo;
    }

    public String getVarnrotelefonoreclamo() {
        return varnrotelefonoreclamo;
    }

    public void setVarnrotelefonoreclamo(String varnrotelefonoreclamo) {
        this.varnrotelefonoreclamo = varnrotelefonoreclamo;
    }

    public String getVarcodigo() {
        return varcodigo;
    }

    public void setVarcodigo(String varcodigo) {
        this.varcodigo = varcodigo;
    }

    public String getVarrecursoapelacion() {
        return varrecursoapelacion;
    }

    public void setVarrecursoapelacion(String varrecursoapelacion) {
        this.varrecursoapelacion = varrecursoapelacion;
    }

    public String getVarfecharesolucion() {
        return varfecharesolucion;
    }

    public void setVarfecharesolucion(String varfecharesolucion) {
        this.varfecharesolucion = varfecharesolucion;
    }

    public String getVarfechanotificacion() {
        return varfechanotificacion;
    }

    public void setVarfechanotificacion(String varfechanotificacion) {
        this.varfechanotificacion = varfechanotificacion;
    }

    public String getVarrazon1() {
        return varrazon1;
    }

    public void setVarrazon1(String varrazon1) {
        this.varrazon1 = varrazon1;
    }

    public String getVarrazon2() {
        return varrazon2;
    }

    public void setVarrazon2(String varrazon2) {
        this.varrazon2 = varrazon2;
    }

    public String getVarrazon3() {
        return varrazon3;
    }

    public void setVarrazon3(String varrazon3) {
        this.varrazon3 = varrazon3;
    }

    public String getVarrazon4() {
        return varrazon4;
    }

    public void setVarrazon4(String varrazon4) {
        this.varrazon4 = varrazon4;
    }

    public String getVarrazon5() {
        return varrazon5;
    }

    public void setVarrazon5(String varrazon5) {
        this.varrazon5 = varrazon5;
    }

    public String getVarrazon6() {
        return varrazon6;
    }

    public void setVarrazon6(String varrazon6) {
        this.varrazon6 = varrazon6;
    }

    public String getVarrazon7() {
        return varrazon7;
    }

    public void setVarrazon7(String varrazon7) {
        this.varrazon7 = varrazon7;
    }

    public String getVarrazon8() {
        return varrazon8;
    }

    public void setVarrazon8(String varrazon8) {
        this.varrazon8 = varrazon8;
    }

    public String getVarrazon9() {
        return varrazon9;
    }

    public void setVarrazon9(String varrazon9) {
        this.varrazon9 = varrazon9;
    }

    public String getVarrazon10() {
        return varrazon10;
    }

    public void setVarrazon10(String varrazon10) {
        this.varrazon10 = varrazon10;
    }

    public String getVarrazon11() {
        return varrazon11;
    }

    public void setVarrazon11(String varrazon11) {
        this.varrazon11 = varrazon11;
    }

    public String getVarrazon12() {
        return varrazon12;
    }

    public void setVarrazon12(String varrazon12) {
        this.varrazon12 = varrazon12;
    }

    public String getVarrazon13() {
        return varrazon13;
    }

    public void setVarrazon13(String varrazon13) {
        this.varrazon13 = varrazon13;
    }

    public String getVarrazon14() {
        return varrazon14;
    }

    public void setVarrazon14(String varrazon14) {
        this.varrazon14 = varrazon14;
    }

    public String getVarrazon15() {
        return varrazon15;
    }

    public void setVarrazon15(String varrazon15) {
        this.varrazon15 = varrazon15;
    }

    public String getVarrazon16() {
        return varrazon16;
    }

    public void setVarrazon16(String varrazon16) {
        this.varrazon16 = varrazon16;
    }

    public String getVarrazon17() {
        return varrazon17;
    }

    public void setVarrazon17(String varrazon17) {
        this.varrazon17 = varrazon17;
    }

    public String getVarfecha() {
        return varfecha;
    }

    public void setVarfecha(String varfecha) {
        this.varfecha = varfecha;
    }
}
